file_path = "input.txt"

with open(file_path) as f:
    lines = f.readlines()

lines = [x.strip() for x in lines]
lines_arr = []
for line in lines:
    line_arr = line.split(" ")
    lines_arr.append(line_arr)

num_lines, num_columns = len(lines_arr), len(lines_arr[0])
results = [[0 for x in range(num_columns)] for y in range(num_lines)]

for j in range(0, num_columns):
    results[0][j] = int(lines_arr[0][j])

for i in range(1, num_lines):
    results[i][0] = min(results[i - 1][0], results[i - 1][1]) + int(lines_arr[i][0])
    results[i][num_columns - 1] = min(results[i - 1][num_columns - 2], results[i - 1][num_columns - 1]) + int(
        lines_arr[i][num_columns - 1])

    for j in range(1, num_columns - 1):
        arr = []
        for k in range(-1, 2):
            arr.append(results[i - 1][j + k])
        results[i][j] = min(arr) + int(lines_arr[i][j])

minimum = results[999][:]

print("Minimum je " + str(min(minimum)))
